﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[RequireComponent (typeof(NetworkIdentity))]

public abstract class Ability : NetworkBehaviour {

	public Texture2D abilityIcon;

	public float maxDeathTimer = 3f; // this obj will be desroyed in 10 seconds after being created
	public float currDeathTimer = 0f;

	public Player caster;
	public Player target;

	void Start () {
		Init ();
	}

	abstract public void Init ();


	// called once per frame
	void Update () {
		if (!base.isServer) {
			return;
		}

		UpdateDeathTimer (); 
		UpdateInstance ();
		//UpdatePosition ();
		//UpdateStatusTimers ();
	}

	void UpdateDeathTimer () {
		if (currDeathTimer > maxDeathTimer) {
			Death ();
		}
		currDeathTimer += Time.deltaTime;
	}

	abstract public void UpdateInstance ();
	
	//abstract public void UpdateStatusTimers ();
	

	public bool ColEqualsCaster(Collider col) {
		return (col.gameObject == caster.gameObject);
	}

	public void Death () {
		Destroy (gameObject);
	}

	void OnGUI() {
		//GUI.DrawTexture (new Rect (10, 150, abilityIcon.width, abilityIcon.height), abilityIcon);
	}



}
