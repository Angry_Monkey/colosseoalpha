﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Frostbolt : Ability {
	public float speed;
	public float slowDuration;
	public float slowTimer;
	public float slowAmount;
	public int damage;


	// Use this for initialization
	override public void Init () {
		speed = 12f;
		slowDuration = 2f;
		slowTimer = 0f;
		slowAmount = 0.5f;
		damage = -10;
	}


	override public void UpdateInstance () {
		if (!target) {
			//  frostbolt moves towards the direction player is facing
			float move = speed * Time.deltaTime;
			transform.Translate (Vector3.forward * move);
		} 
	}
	
	
	void OnTriggerEnter(Collider col) {
		if (!isServer) {
			return;
		}

		string tag = col.gameObject.tag;
		if (tag == "Player") {

			if (ColEqualsCaster (col)) {
				return;
			}

			target = col.GetComponent<Player> ();

			target.UpdateHealth (damage);

			target.Slow (slowAmount, slowDuration);

			Death();
		}
	}






}



