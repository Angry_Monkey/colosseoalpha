﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[RequireComponent (typeof(LineRenderer))]

public class ArcaneBeam : Ability {
	public Vector3 origin;
	public Vector3 destination;
	
	float width;
	float speed;
	//Color color = Color.clear;
	
	
	LineRenderer lineRenderer;
	float length;

	float duration;
	float durationTimer;

	float maxTimer;
	float timer;

	// Use this for initialization
	override public void Init () {
		if (!isServer) {
			return;
		}
		caster.Root (2f);
		caster.currRotateSpeed = 30;

		width = 0.5f;
		length = 30;
		speed = 60;

		duration = 2f;
		durationTimer = 0f;

		maxTimer = 0.1f;
		timer = 0f;

		origin = caster.transform.position;
		destination = origin;

		lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.SetWidth(width, width);
		lineRenderer.SetVertexCount (2);
		//lineRenderer.material = new Material("Arcane.mat");
		//lineRenderer.material = Resources.Load("Arcane.mat", typeof(Material)) as Material;
		//lineRenderer.SetColors(color,color);
	}



	override public void UpdateInstance() {
		if (!isServer) {
			return;
		}
		origin = caster.transform.position;
		lineRenderer.SetPosition (0, origin);


		//Shoot our laserbeam forwards!
		UpdateLength();
		lineRenderer.SetPosition (1, destination);

		RpcPositions (origin, destination);

		UpdateStatusTimers ();

	}

	[ClientRpc]
	void RpcPositions(Vector3 ori, Vector3 des) {
		if (!lineRenderer) {
			lineRenderer = GetComponent<LineRenderer> ();
		}
		this.lineRenderer.SetPosition (0, ori);
		this.lineRenderer.SetPosition (1, des);
		this.lineRenderer.SetWidth(0.5f, 0.5f);
	}

	void UpdateStatusTimers() {
		if (durationTimer < duration) {
			Debug.Log(durationTimer);
			durationTimer += Time.deltaTime;
		} else {
			caster.currRotateSpeed = caster.maxRotateSpeed;
			Death();
		}
	}

	
	void UpdateLength() {
		if (timer <= 0) {
			//Raycast from the location of the cube forwards
			RaycastHit[] hit;
			hit = Physics.RaycastAll (caster.transform.position, caster.transform.forward, length);
			int i = 0;
			while (i < hit.Length) {
				//Check to make sure we aren't hitting triggers but colliders
				if (!hit [i].collider.isTrigger) {

					if (hit [i].collider.tag == "Player" && hit [i].collider.gameObject != caster.gameObject) {

						hit [i].collider.GetComponent<Player> ().UpdateHealth (-2);
						timer = maxTimer;
					}
					

				} 
				i++;
			}
		
		} else {
			timer -= Time.deltaTime;
		}
	
		destination = origin + caster.transform.forward * durationTimer * speed;
	}
}