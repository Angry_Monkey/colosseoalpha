﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;

public class End : MonoBehaviour {

	public Text isWin;
	public Text scoreText;
	public Text notice;
	public Button toMenu;
	GameState gamestate;
	string url;

	// Use this for initialization
	void Start () {


		toMenu.onClick.AddListener (() =>{
			Application.LoadLevel(1);
		});



		gamestate = GameObject.Find ("Message").GetComponent<GameState> ();

		url = "https://colosseo.herokuapp.com/api/users/" + gamestate.id.ToString ();

		int newScore = gamestate.score;

		if (gamestate.isWinner) {
			isWin.text = "You Win!!";
			newScore += 1;
		} else {
			isWin.text = "Game Over";
		}

		scoreText.text = "Total score: " + newScore.ToString();

		string jdata = "{\"user\":{\"score\":\""+newScore+"\"}}";

		byte[] data = System.Text.Encoding.UTF8.GetBytes (jdata);

		StartCoroutine(makeConnection (url, data, notice));

	
	}

	public IEnumerator makeConnection(string url, byte[] data, Text notice) {
		
		Dictionary<string, string> header = new Dictionary<string, string>();
		
		header.Add("Content-type","application/json");

		header.Add("Authorization","Token token=\""+gamestate.token+"\"");
		
		WWW connection = new WWW (url, data, header);
		
		yield return connection;
		
		if (!string.IsNullOrEmpty (connection.error)) {
			var response = JSON.Parse(connection.text);
			string errors="";
			foreach(string key in response.Keys) {
				errors+=(key+": "+response[key]+"\n");
			}
			notice.text = errors;
		} 
		else {
			var response = JSON.Parse(connection.text);

			Debug.Log(response["score"]);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
