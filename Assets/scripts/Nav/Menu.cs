﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public Button class1;
	public Button class2;
	public Button class3;
	public Button exit;
	public Text userName;
	public GameState gamestate;

	Color fire = new Color (1f,0.33f,0f);
	Color frost = new Color(0f,0.74f,1f);
	Color arcane = new Color(0.83f,0.27f,1f);
	
	float maxTimer = 0.2f;
	float timer = 0f;
	float multiplier = 0.6f;

	Renderer cube;
	ParticleSystem ps;
	Light l;


	Color currentColor;
	Color targetColor;
	float colorTimer = 1;

	public Text selected;
	
	// Use this for initialization
	void Start () {



		gamestate = GameObject.Find ("Message").GetComponent<GameState> ();

		cube = GameObject.Find ("MenuCube").GetComponent<Renderer> ();
		ps = GameObject.Find ("MenuParticles").GetComponent<ParticleSystem> ();
		l = GameObject.Find ("Directional Light").GetComponent<Light> ();

		currentColor = l.color;
		targetColor = currentColor;

		Text t1 = class1.GetComponentInChildren<Text> ();
		Text t2 = class2.GetComponentInChildren<Text> ();
		Text t3 = class3.GetComponentInChildren<Text> ();

		userName.text = "Hello " + gamestate.username;

		class1.onClick.AddListener (() =>{
			ps.startColor = fire;
			t1.color = fire;
			if (selected) {
				selected.color = Color.gray;
			}
			selected = t1;
			gamestate.myClass = 0;
			gamestate.color = fire;
			targetColor = fire;
			colorTimer = 0f;
			//cube.transform.localScale *= 2;
		});
		class2.onClick.AddListener (() =>{
			ps.startColor = frost;
			t2.color = frost;
			if (selected) {
				selected.color = Color.grey;
			}
			selected = t2;
			gamestate.myClass = 1;
			gamestate.color = frost;
			targetColor = frost;
			colorTimer = 0f;
		});
		class3.onClick.AddListener (() =>{
			ps.startColor = arcane;
			t3.color = arcane;
			if (selected) {
				selected.color = Color.grey;
			}
			selected = t3;
			gamestate.myClass = 2;
			gamestate.color = arcane;
			targetColor = arcane;
			colorTimer = 0f;
		});
		//class2.onClick.AddListener (() =>{Application.LoadLevel(1);});
		exit.onClick.AddListener (() =>{Application.Quit();});

	}




	// Update is called once per frame
	void Update () {

		if (selected) {
			if (timer < maxTimer) {
				timer += Time.deltaTime;
			} else {
				timer = 0;
				multiplier *= -1;
			}
			cube.transform.localScale *= (1 + Time.deltaTime * multiplier);
		}

		if (colorTimer < 1) {
			currentColor = Color.Lerp (currentColor, targetColor, colorTimer);
			l.color = currentColor;
			cube.material.color = l.color;
			colorTimer += (Time.deltaTime / 5);
		} else {
			currentColor = targetColor;
		}

	}







}
