﻿using UnityEngine;
using System.Collections;

public class Push : Ability {
	public float pushDist;
	public float speed;
	//public float stunDuration;
	//public float stunTimer;
	public int damage;

	public float travelDistance;
	public float maxTravelDistance;
	
	// Use this for initialization
	override public void Init () {
		pushDist = 7f;
		speed = 15f;
		damage = -5;
		travelDistance = 0;
		maxTravelDistance = 7f;
		GetComponent<AudioSource> ().Play ();
	}
	
	
	override public void UpdateInstance () {

		// moves towards the direction player is facing
		float move = speed * Time.deltaTime;
		transform.Translate (Vector3.forward * move);
		transform.localScale += new Vector3 (Time.deltaTime * 10, 0, 0);
		travelDistance += move;
		if (travelDistance > maxTravelDistance) {
			Death();
		}
	}
	
	void OnTriggerEnter(Collider col) {
		if (!isServer) {
			return;
		}
		
		string tag = col.gameObject.tag;
		if (tag == "Player") {

			if (ColEqualsCaster (col)) {
				return;
			}

			
			target = col.GetComponent<Player>();
			
			target.UpdateHealth(damage);
			
			Vector3 translation = (transform.forward) * pushDist;
			target.Push(transform.position + translation);
			

		}
	}
}
