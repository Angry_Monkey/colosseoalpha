﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class ArcaneOrb : Ability {


	public float speed;
	public float slowDuration;
	public float slowTimer;
	public float slowAmount;
	public int damage;
	
	
	// Use this for initialization
	override public void Init () {
		if (!isServer) {
			return;
		}

		speed = 12f;
		slowDuration = 2f;
		slowTimer = 0f;

		damage = -10 - 2 * caster.arcaneStack;
		transform.localScale *= (1 + caster.arcaneStack * 0.2f);

		RpcScale (transform.localScale);

	}
	
	
	override public void UpdateInstance () {
		if (!isServer) {
			return;
		}

		if (!target) {
			//  frostbolt moves towards the direction player is facing
			float move = speed * Time.deltaTime;
			transform.Translate (Vector3.forward * move);
		} 
	}
	[ClientRpc]
	void RpcScale(Vector3 scale) {
		transform.localScale = scale;
	}
	
	
	void OnTriggerEnter(Collider col) {
		if (!isServer) {
			return;
		}
		
		string tag = col.gameObject.tag;
		if (tag == "Player") {
			
			if (ColEqualsCaster (col)) {
				return;
			}
			
			target = col.GetComponent<Player> ();
			target.UpdateHealth (damage);
			caster.arcaneStack += 1;
			Death();
		}
	}

}
