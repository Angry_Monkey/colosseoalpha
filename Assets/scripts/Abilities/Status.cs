﻿using UnityEngine;
using System.Collections;

public class Status : MonoBehaviour {


	public Player target;
	public float maxDeathTimer = 10f;
	public float currDeathTimer = 0f;
	public float timer;
	public float currTimer;
	public float amount;
	// Use this for initialization
	void Start () {
	
	}

	void InitAndSetTime (Player player, float timer, float amount) {
		this.target = player;
		this.timer = timer;
		this.currTimer = 0;
		this.amount = amount;
	}

	// Update is called once per frame
	void Update () {
		if (currDeathTimer > maxDeathTimer || currTimer > timer) {
			Destroy (gameObject);
		}
		currTimer += Time.deltaTime;
		currDeathTimer += Time.deltaTime;
	}

}
