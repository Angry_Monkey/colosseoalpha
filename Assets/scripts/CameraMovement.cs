﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
	public Transform Player;
	public Vector3 Offset;
	

	void LateUpdate ()
	{
		if (Player != null)
			transform.position = Player.position + Offset;
	}

	public void SetPlayer(Transform t) {
		Player = t;
	}
}
