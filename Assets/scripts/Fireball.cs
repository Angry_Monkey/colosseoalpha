﻿using UnityEngine;
using System.Collections;

public class Fireball : Ability {
	public float dist;
	public float speed;
	//public float stunDuration;
	//public float stunTimer;
	public int damage;
	
	// Use this for initialization
	override public void Init () {
		dist = 4f;
		speed = 12f;
		damage = -10;
		GetComponent<AudioSource> ().Play ();
	}


	override public void UpdateInstance () {

		if (!target) {
			//  fireball moves towards the direction player is facing
			float move = speed * Time.deltaTime;
			transform.Translate (Vector3.forward * move);
		} 

	}

	void OnTriggerEnter(Collider col) {
		if (!isServer) {
			return;
		}

		string tag = col.gameObject.tag;
		if (tag == "Player") {

			if (ColEqualsCaster (col)) {
				return;
			}

			target = col.GetComponent<Player>();

			target.UpdateHealth(damage);

			Vector3 translation = (transform.forward) * dist;
			target.Push(transform.position + translation);
		
			Death ();
		}
	}

}
