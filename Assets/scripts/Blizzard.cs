﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Blizzard : Ability {
	float factor = 1.5f;
	float speed = 3f;

	float maxTimer = 0.1f;
	float timer = 0;

	public Vector3 dir;

	// Use this for initialization
	override public void Init() {
		base.maxDeathTimer = 6f;
		dir = transform.forward;
		transform.position += dir * 2;
		GetComponent<AudioSource> ().Play ();
	}

	override public void UpdateInstance () {
		transform.Rotate (0, 5, 0, Space.World);
		transform.localScale += new Vector3(Time.deltaTime*factor, 0, Time.deltaTime*factor);
		RpcScale (this.transform.localScale);

		float move = speed * Time.deltaTime;
		transform.Translate (dir * move, Space.World);
		//transform.Translate (dir * move, Space.World);
	}

	[ClientRpc]
	void RpcScale(Vector3 scale) {
		transform.localScale = scale;
	}

	void OnTriggerEnter(Collider col) {
		string tag = col.gameObject.tag;
		if (tag == "Player") {
			if (ColEqualsCaster (col)) {
				return;
			}
			target = col.GetComponent<Player> ();
			target.Push (transform.position);
		}
	}

	void OnTriggerStay(Collider col) {
		if (!isServer) {
			return;
		}
		
		string tag = col.gameObject.tag;
		if (tag == "Player") {

			if (ColEqualsCaster (col)) {
				return;
			}

			target = col.GetComponent<Player>();


			if (timer == 0) {
				target.UpdateHealth(-1);
				target.Slow(0.5f, 0.5f);
				timer+=Time.deltaTime;
			}
			else if (timer < maxTimer) {
				timer+=Time.deltaTime;
			}
			else {
				timer=0;
			}

		}
	}
}
