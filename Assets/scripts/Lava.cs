﻿using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {

	private int damage = -1;
	private float timer;
	private float maxTimer;

	// Use this for initialization
	void Start () {
		maxTimer = 0.12f;
		timer = 0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider col) {

	}

	void OnTriggerStay (Collider col) {

		string tag = col.gameObject.tag;
		if (tag == "Player") {
			Player p = col.GetComponent<Player>();

			if (timer <= 0) {
				p.UpdateHealth(damage);
				p.Slow(0.6f, 0.12f);
				timer = maxTimer;
			} else {
				timer -= Time.deltaTime;
			}

		}
	}

	void OnTriggerExit (Collider col) {

	}

}
