﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SignUp : Session {
	
	public InputField username;
	public InputField password;
	public Text notice;
	
	private string url = "https://colosseo.herokuapp.com/api/signup";
	
	// Use this for initialization
	
	public void doSignUp() {
		
		string usernameInput = username.text;
		string passwordInput = password.text;
		
		string jdata = "{\"user\":{\"username\":\""+usernameInput+"\",\"password\":\""+passwordInput+"\"}}";
		
		byte[] data = System.Text.Encoding.UTF8.GetBytes (jdata);
		
		StartCoroutine(makeConnection (url, data, notice));
		
	}
}
