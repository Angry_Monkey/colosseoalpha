﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;

public class Session : MonoBehaviour {
	private string token = "";
	private string id = "";

	public string getToken() {
		return token;
	}
	public void setToken(string token) {
		this.token = token;
	}

	public string getId() {
		return id;
	}
	public void setId(string id) {
		this.id = id;
	}

	public IEnumerator makeConnection(string url, byte[] data, Text notice) {
		
		Dictionary<string, string> header = new Dictionary<string, string>();

		GameState gamestate = GameObject.Find ("Message").GetComponent<GameState> ();

		header.Add("Content-type","application/json");
		
		WWW connection = new WWW (url, data, header);
		
		yield return connection;

		if (!string.IsNullOrEmpty (connection.error)) {
			var response = JSON.Parse(connection.text);
			string errors="";
			foreach(string key in response.Keys) {
				errors+=(key+": "+response[key]+"\n");
			}
			notice.text = errors;
		} 
		else {
			var response = JSON.Parse(connection.text);
			setToken(response["token"]);
			setId(response["id"]);
			string score = response["score"];

			notice.text = "";

			gamestate.id = getId();
			gamestate.token = getToken();
			gamestate.score = int.Parse(score);
			gamestate.username = response["username"];


			Debug.Log(gamestate.id);
			Debug.Log(gamestate.token);
			Debug.Log(gamestate.score);

			// switch to menu scene
			Application.LoadLevel (1);

		}
	}
}
