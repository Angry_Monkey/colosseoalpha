﻿using UnityEngine;
using System.Collections;

public class Shield : Ability {
	
	private float duration;
	private float timer;


	// Use this for initialization
	override public void Init() {
		if (!isServer) {
			return;
		}
		duration = 2f;
		timer = 0f;

		caster.shieldAmount = 20;

	}


	
	override public void UpdateInstance () {
		if (!isServer) {
			return;
		}
		if (caster.shieldAmount <= 0) {
			Death();
		}
		transform.position = caster.transform.position;

		if (timer < duration) {
			timer += Time.deltaTime;
		} else {
			caster.shieldAmount = 0;
			Death ();
		}

	}

	/*
	void OnTriggerEnter(Collider col) {
		string name = col.gameObject.name;
		if (name != "Player") {
			Destroy(col);
		}
	}
	*/




}
