﻿using UnityEngine;
using System.Collections;

public class Ladder : MonoBehaviour {
	private int forceMagnitude = 350;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col) {
		string tag = col.gameObject.tag;
		if (tag == "Player") {
			col.GetComponent<Rigidbody>().AddForce(Vector3.up * forceMagnitude);
		}
	}
}
