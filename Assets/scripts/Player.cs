using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;

public class Player : NetworkBehaviour {

	// basic attribute for player
	public int maxHealth;
	[SyncVar] public int currHealth;
	public float maxMoveSpeed;
	[SyncVar] public float currMoveSpeed;
	public int maxRotateSpeed;
	[SyncVar] public int currRotateSpeed;

	[SyncVar] public bool isPushed;
	float pushSpeed;

	[SyncVar] public int shieldAmount;


	// move function need these
	bool finishedMoving;
	[SyncVar] Vector3 destinationPosition;		// The destination Point
	public float destinationDistance;			// The distance between myTransform and destinationPosition
	Quaternion targetRotation;

	[SyncVar] public bool isSilenced = false;
	[SyncVar] public bool isRooted = false;
	[SyncVar] public bool isSlowed = false;
	[SyncVar] public bool isStunned = false;

	public float silenceTimer;
	public float rootTimer;
	public float slowTimer;
	public float stunTimer;

	[SyncVar] public bool isDead = false;
	public float respawnTimer = 3f;
	
	public float healthBarLength;
	public Texture healthBarTexture;

	public Text statusText;
	public Text overText;
	public RawImage icon1;
	public RawImage icon2;
	public Button icon1Button;
	public Image cdSprite1;
	public Image cdSprite2;
	public Text cdText1;
	public Text cdText2;

	public Ability[] abilities;
	public Shield shield;

	public float globalCd;
	public float globalCdTimer;

	public float abilit1Cd;
	public float ability1CdTimer;
	public float abilit2Cd;
	public float ability2CdTimer;
	public float abilit3Cd;
	public float ability3CdTimer;
	public int arcaneStack = 0;

	public Color classColor;
	[SyncVar] public int classId;

	public GameState gamestate;

		
	// Use this for initialization
	public override void OnStartLocalPlayer () {
		base.OnStartLocalPlayer ();

		AttachCamera ();
		AttachCanvas ();

		gamestate = GameObject.Find ("Message").GetComponent<GameState> ();
		classColor = gamestate.color;
		classId = gamestate.myClass;
		GetComponent<Renderer> ().material.color = classColor;
		SetAbilities ();
		CmdInitClass (classColor, classId);

		maxRotateSpeed = 1440;
		currRotateSpeed = maxRotateSpeed;
		maxMoveSpeed = 6f;
		currMoveSpeed = maxMoveSpeed;
		
		maxHealth = 100;
		currHealth = maxHealth;

		shieldAmount = 0;

		destinationPosition = transform.position;
		finishedMoving = true;

		isPushed = false;
		pushSpeed = 6;

		silenceTimer = 0;
		rootTimer = 0;
		slowTimer = 0;
		stunTimer = 0;



		cdSprite1.fillAmount = 0f;
		cdText1.text = "";
		cdSprite2.fillAmount = 0f;
		cdText2.text = "";
	}

	void Start() {
		healthBarLength = Screen.width / 6;
		if (!isLocalPlayer) {
			//CmdClassColor();
			if (classId == 0) {
				GetComponent<Renderer> ().material.color = new Color (1f,0.33f,0f);
			} else if (classId == 1) {
				GetComponent<Renderer> ().material.color = new Color(0f,0.74f,1f);
			} else if (classId == 2) {
				GetComponent<Renderer> ().material.color = new Color(0.83f,0.27f,1f);
			} 
		}

	}

	private void AttachCamera() {
		GameObject.Find ("Main Camera").GetComponent<CameraMovement> ().SetPlayer (transform);
		//GameObject.Find ("Icon1").GetComponent<DragAbility> ().setPlayer(this);
		//GameObject.Find ("Icon2").GetComponent<DragAbility1> ().setPlayer(this);
	}

	void AttachCanvas() {

		overText = GameObject.Find ("Over").GetComponent<Text> ();
		statusText = GameObject.Find ("Status").GetComponent<Text> ();

		icon1 = GameObject.Find ("Icon1").GetComponent<RawImage> ();
		cdSprite1 = GameObject.Find ("CdSprite1").GetComponent<Image> ();
		cdText1 = GameObject.Find ("CdText1").GetComponent<Text> ();

		icon2 = GameObject.Find ("Icon2").GetComponent<RawImage> ();
		cdSprite2 = GameObject.Find ("CdSprite2").GetComponent<Image> ();
		cdText2 = GameObject.Find ("CdText2").GetComponent<Text> ();
	}


	[Command]
	void CmdInitClass(Color c, int id) {
		classId = id;
		classColor = c;
		GetComponent<Renderer> ().material.color = c;
		SetAbilities ();

		//RpcInitClass (c, id);
	}

	[ClientRpc]
	void RpcInitClass(Color c, int id) {
		classId = id;
		GetComponent<Renderer> ().material.color = c;
	//	SetAbilities ();
		//Debug.Log (this.playerControllerId);
	}

	void SetAbilities () {
		globalCd = 0.5f;
		globalCdTimer = 0f;
		abilit1Cd = 0.5f;
		ability1CdTimer = 0f;
		abilit2Cd = 6f;
		ability2CdTimer = 0f;
		abilit3Cd = 60f;
		ability3CdTimer = 0f;

		if (classId == 0) {
			abilities [0] = (Fireball)Resources.Load ("prefabs/Fireball", typeof(Fireball)); 
			abilities [1] = (Push)Resources.Load ("prefabs/Push", typeof(Push)); 
			//icon.texture = (Texture)Resources.Load ("pictures/fireball", typeof(Texture));
		} else if (classId == 1) {
			abilities [0] = (Frostbolt)Resources.Load ("prefabs/Frostbolt", typeof(Frostbolt));
			abilities[1] = (Blizzard)Resources.Load("prefabs/BlizzardTrigger", typeof(Blizzard));
			//icon.texture = (Texture)Resources.Load ("pictures/frostbolt", typeof(Texture)); 
		} else if (classId == 2) {
			abilities [0] = (ArcaneOrb)Resources.Load ("prefabs/ArcaneOrb", typeof(ArcaneOrb));
			abilities [1] = (ArcaneBeam)Resources.Load ("prefabs/ArcaneBeam", typeof(ArcaneBeam));
			//icon.texture = (Texture)Resources.Load ("pictures/arcane orb", typeof(Texture)); 

		}
		SetAbilitIcons ();
	}

	void SetAbilitIcons() {
		if (!isLocalPlayer) {
			return;
		}
		if (classId == 0) {
			icon1.texture = (Texture)Resources.Load ("pictures/fireball", typeof(Texture));
			icon2.texture = (Texture)Resources.Load ("pictures/Push", typeof(Texture));
		} else if (classId == 1) {
			icon1.texture = (Texture)Resources.Load ("pictures/frostbolt", typeof(Texture)); 
			icon2.texture = (Texture)Resources.Load ("pictures/blizzard", typeof(Texture)); 
		} else if (classId == 2) {
			icon1.texture = (Texture)Resources.Load ("pictures/ArcaneOrb", typeof(Texture)); 
			icon2.texture = (Texture)Resources.Load ("pictures/ArcaneBeam", typeof(Texture)); 
		}
	}

	
	// Update is called once per frame
	private void Update () {

		if (isServer) {
			UpdateStatusTimers ();
		} 

		if (isLocalPlayer) {
			//Debug.Log (currHealth);
			if (currHealth <= 0) {
				Death();
				return;
			}
			UpdateCdTimers ();
			UpdateCdIcons();
			UpdateMovement ();
		}

	}



	void UpdateStatusTimers() {
		//Debug.Log (slowTimer);

		if (stunTimer > 0) {
			stunTimer -= Time.deltaTime;
			if (stunTimer <= 0) {
				isStunned = false;
			}
		}
		if (silenceTimer > 0) {
			silenceTimer -= Time.deltaTime;
			if (silenceTimer <= 0) {
				isSilenced = false;
			}
		}
		if (rootTimer > 0) {
			rootTimer -= Time.deltaTime;
			if (rootTimer <= 0) {
				isRooted = false;
			}
		}
		if (slowTimer > 0) {
			slowTimer -= Time.deltaTime;
			if (slowTimer <= 0) {
				isSlowed = false;
			}
		}
	}


	void UpdateMovement() {

		statusText.text = "";

		if (isPushed) {
			UpdatePush();
			return;
		}
		
		if (isStunned) {
			statusText.text = "Stunned";
			return;
		} 
		
		if (!isSilenced) {
			CastAbilities ();
		} else {
			statusText.text = "Silenced";
		}
		
		if (isRooted) {
			statusText.text = "Rooted";
			Move (0f);
			return;
		} 
		
		if (!isSlowed) {
			currMoveSpeed = maxMoveSpeed;
		} 
		Move (currMoveSpeed);
	}


	void UpdatePush() {
		destinationDistance = Vector3.Distance (destinationPosition, transform.position);
		if (destinationDistance < 0.5f) {
			CmdUnpush();
		} else {
			Debug.Log(destinationPosition);
			transform.position = 
				Vector3.Lerp(transform.position, destinationPosition, 
				             pushSpeed * Time.deltaTime);
		}
	}

	[Command]
	void CmdUnpush() {
		isPushed = false;
	}


	void Move(float speed) {
		if (!isLocalPlayer) {
			return;
		}
		// keep track of the distance between this gameObject and destinationPosition
		destinationDistance = Vector3.Distance (destinationPosition, transform.position);
		
		if (destinationDistance < .5f) {		// To prevent shakin behavior when near destination
			//speed = 0f;
			finishedMoving = true;
		}
		
		// Moves the Player if the Left Mouse Button was clicked
		if (Input.GetMouseButtonDown(0)) {
			//print(moveSpeed);
			
			Plane playerPlane = new Plane(Vector3.up, transform.position);
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float hitdist = 0.0f;
			
			if (playerPlane.Raycast(ray, out hitdist)) {
				Vector3 targetPoint = ray.GetPoint(hitdist);
				destinationPosition = ray.GetPoint(hitdist);
				targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
			}
			finishedMoving = false;
		}
		// Moves the player if the mouse button is hold down

		else if (Input.GetMouseButton(0)&& GUIUtility.hotControl ==0) {
			
			Plane playerPlane = new Plane(Vector3.up, transform.position);
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float hitdist = 0.0f;
			
			if (playerPlane.Raycast(ray, out hitdist)) {
				Vector3 targetPoint = ray.GetPoint(hitdist);
				destinationPosition = ray.GetPoint(hitdist);
				targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

			}
			finishedMoving = false;
			
		} 

		
		if (!finishedMoving) {
			transform.position = Vector3.MoveTowards (transform.position, destinationPosition, speed * Time.deltaTime);


			Quaternion limitedRotation = new Quaternion(0F,0F,0F,0F);      
			//lock rotation, yaw only
			targetRotation.eulerAngles = new Vector3(0,targetRotation.eulerAngles.y,0);
			
			//rotates the Ship to Reticle with fixed speed
			limitedRotation = Quaternion.RotateTowards(transform.rotation, targetRotation, currRotateSpeed * Time.deltaTime);
			
			transform.rotation = limitedRotation;
			//transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * currRotateSpeed);
		}
	}



	//local player
	void CastAbilities() {

		if (globalCdTimer > 0) {
			return;
		}

		if (Input.GetKeyDown (KeyCode.Space) && ability1CdTimer <= 0) {
				//ability1.caster = this;
				//abilities[0].caster = this;
			Cmd_CastAbility(0);

			globalCdTimer = globalCd;
			ability1CdTimer = abilit1Cd;
				//ability1.Cmd_Cast ();
		}
		if (Input.GetKeyDown (KeyCode.Q) && ability2CdTimer <= 0) {
			Cmd_CastAbility(1);
			globalCdTimer = globalCd;
			ability2CdTimer = abilit2Cd;
		}
		if (Input.GetKeyDown (KeyCode.W) && ability3CdTimer <= 0) {
			Cmd_CastAbility(2);
			globalCdTimer = globalCd;
			ability3CdTimer = abilit3Cd;
				//ability3.caster = this;
				//ability3.Cmd_Cast ();
		}
	}

	[Command]
	public void Cmd_CastAbility(int n) {
		GameObject obj = (GameObject)Instantiate (abilities[n].gameObject, transform.position, transform.rotation);
		obj.GetComponent<Ability> ().caster = this;
		NetworkServer.Spawn(obj);

		if (classId == 2 && n == 1) {
			GameObject obj2 = (GameObject)Instantiate (shield.gameObject, transform.position, transform.rotation);
			obj2.GetComponent<Ability> ().caster = this;
			NetworkServer.Spawn(obj2);
		}
	}

	public void UpdateHealth (int amount) {
		if (!isServer) {
			return;
		}
		if (amount < 0) {
			arcaneStack = 0;
		}

		int amountAfterShield = shieldAmount + amount;
		if (amountAfterShield < 0) {
			currHealth += shieldAmount + amount;
		} else {
			shieldAmount += amount;
		}

	}

	void OnGUI() {
		Vector2 targetPos = Camera.main.WorldToScreenPoint (transform.position);	

		GUI.DrawTexture(new Rect(targetPos.x, Screen.height - targetPos.y, 100 * (float)(currHealth + shieldAmount) / (maxHealth + shieldAmount), 20), healthBarTexture, ScaleMode.ScaleAndCrop, true, 0.0f);
		GUI.backgroundColor = new Color (1.0f, 1.0f, 1.0f, 0.2f);
		GUI.contentColor = Color.black;
		GUI.Box(new Rect(targetPos.x, Screen.height - targetPos.y, 100, 20), (currHealth+shieldAmount) + "/" + (maxHealth + shieldAmount));	

	}


	

	
	void Death() {
		//CmdSetDead ();
		/*
		if (respawnTimer > 0) {
			respawnTimer -= Time.deltaTime;
			overText.text = "Game Over\n" + respawnTimer.ToString("0.0") ;
		} else {
			Application.LoadLevel (0);
		}*/

		gamestate = GameObject.Find ("Message").GetComponent<GameState> ();
		gamestate.isWinner = false;

//		this.connectionToServer.Disconnect ();
//		this.connectionToClient.Disconnect ();
	//	NetworkManager nm = GameObject.Find ("NetworkManager").GetComponent<NetworkManager> ();
	//	nm.StopHost ();
		//nm.manager.StopHost ();

//		nm.showGUI = false; 
		CameraMovement cm = GameObject.Find ("Main Camera").GetComponent<CameraMovement> ();
		//cm.Offset = new Vector3 (0, 20, -8);

		Destroy (this.gameObject);

		cm.transform.position = new Vector3 (0, 20, -8);


		//GetComponent<Renderer> ().enabled = false;
		//GetComponent<Collider> ().enabled = false;
		//Application.LoadLevel (2);
	}

	[Command]
	void CmdSetDead() {
		isDead = true;
	}

	public void Stun(float duration) {
		if (!isServer) {
			return;
		}
		isStunned = true;
		if (duration > stunTimer) {
			stunTimer = duration;
		}
	}


	public void Slow(float slowAmount, float duration) {
		if (!isServer) {
			return;
		}
		isSlowed = true;
		if (duration > slowTimer) {
			currMoveSpeed = maxMoveSpeed * (1 - slowAmount);
			slowTimer = duration;
		}
	}


	public void Root(float duration) {
		if (!isServer) {
			return;
		}
		isRooted = true;
		if (duration > rootTimer) {
			rootTimer = duration;
		}
	}
	
	public void Push(Vector3 destination) {
		if (!isServer) {
			return;
		}
		//GetComponent<Rigidbody> ().AddForce (force);
		isPushed = true;
		//Debug.Log (isPushed);
		destinationPosition = destination;
	}

	/// <summary>
	/// //////////////////////////////////////
	/// </summary>
	public void UpdateCdTimers() {
		//server
		if (globalCdTimer > 0) {
			globalCdTimer -= Time.deltaTime;
		}
		if (ability1CdTimer > 0) {
			ability1CdTimer -= Time.deltaTime;
		} 
		if (ability2CdTimer > 0) {
			ability2CdTimer -= Time.deltaTime;
		}
		if (ability3CdTimer > 0) {
			ability3CdTimer -= Time.deltaTime;
		}
	}

	void UpdateCdIcons() {
		//local player
		if (ability1CdTimer <= 0) {
			cdText1.text = "";
		} else {
			cdSprite1.fillAmount = ability1CdTimer / abilit1Cd;
			cdText1.text = (ability1CdTimer+0.5).ToString ("0");
		}

		if (ability2CdTimer <= 0) {
			cdText2.text = "";
		} else {
			cdSprite2.fillAmount = ability2CdTimer / abilit2Cd;
			cdText2.text = (ability2CdTimer+0.5).ToString ("0");
		}
	}


}
