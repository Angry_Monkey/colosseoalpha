# Colosseo
#####Colosseo front end game application

##Introduction
Colosseo is a cool stage fight game. There are three classes to choose from and each has a unique set of abilities. The goal is to defeat all other players and survive till the end.

Codes in this repository are written in C# and are used by Unity to build the Colosseo game application.

**For documentations and source codes of Colosseo API and game backend, please visit the GitHub page: <https://github.com/seacen/Colosseo>**


##Account
Login is required before playing the game. This is for the purpose of recording individual player's gamescore, which will be shown on the class-choosing menu. To create an account, simply **fill out the form in the same page** and press "Sign-up" button. 


##Game Control
Player movement: mouse click/drag

Cast ability:  ability1: space       ability2: Q


##Classes

###Arcanist
Accumulates powerful arcane energy to destroy his enemies.

Arcane Orb: Increase damage and radius of your next Arcane Orb upon hitting an enemy. Lose all stacks when you are hit.

Aracen Beam: Gain a shield. Channel to shoot a beam forward dealing damage to all enemies in the way. Cannot move when channelling. 

###Cryomancer
Freezes his enemies with frost magic.  

Frostbolt: Slow and damage the enemy hit.

Blizzard: Create a blizzard that grows in size while moving forward slowly. Pull enemie to the center upon contact, slowing and damaging them while they are in area of effects.

###Pyromancer
Manipulates shocking fire to strike enemies away.

Fireball: Damage the push the enemy away.

Dragon's Breath: Enemies in a cone in front of the caster take damage and are pushed away. Enemies near the caster will be pushed further.


##Map
A square arena floating on lava. Note that players will be slowed and damaged upon contact with lava. If fall in lava, a player can move to the edge of arena to jump up back.    

##Testing
Please visit the testing scene under [/Assets/scene/Testing.unity](/Assets/scene/Testing.unity)